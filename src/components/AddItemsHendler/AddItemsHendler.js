import React from 'react';

let hamburgerImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUmX0GItm8Oc0q3AVEA4k9zvQmFtqLQFnvHR5Ruvl7nwklSa0q";
let cheeseburgerImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Cheeseburger.jpg/1200px-Cheeseburger.jpg";
let friesImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLLCQ6qb6lFtB6MaJVv-yvPqUhdbRE5i5av7rYzaH3-0RApksq";
let coffeeImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/A_small_cup_of_coffee.JPG/1200px-A_small_cup_of_coffee.JPG";
let teaImage = "https://cdn2.stylecraze.com/wp-content/uploads/2015/04/2072_11-Surprising-Benefits-And-Uses-Of-Marijuana-Tea_shutterstock_231770824.jpg";
let colaImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRq8fDGmXCqe-f0c44TLKfDrBOxO4G2OFLk0pKUpy-9lZJjsbi2";


const items = [
    {name: 'Hamburger', price: 100, image: hamburgerImage},
    {name: 'Cheeseburger', price: 120, image: cheeseburgerImage},
    {name: 'Fries', price: 50, image: friesImage},
    {name: 'Coffee', price: 30, image: coffeeImage},
    {name: 'Tea', price: 20, image: teaImage},
    {name: 'Cola', price: 40, image: colaImage}
];

const getItemsHendlers = props =>

    <div className="items"><h2>Add items:</h2>
        {items.map((item, key) =>
            <div key={key} className='wrapper'>
                <button onClick={() => props.addClickHandler(item)} className='btn'>
                    <img className='butt' src={item.image} alt={item.name}/>
                    <p>{item.name}</p>
                 </button>
                <span className='priceSpan'>Price: {item.price} KGS</span>
            </div>
        )
        }
    </div>;




export default getItemsHendlers;