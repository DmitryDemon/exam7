import React from 'react';


let remuvePic ="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpLcpYhrWbSCHxr8fULkUdMv3jVz_P0TOG2PmAY-79U4oRR1AP";

const CreateOrder = props =>

    <div className="order"><h2>Order Dettail:</h2>


        {props.items.map((item,key) =>
        <div key={key}>
            {props.items[key].count > 0?
                <div>
                    <p><span className='ttpSpan'>{props.items[key].name}
                                                                    <span className='spanX'>X{props.items[key].count}</span>
                                                                    <span className='priceCurrent'>{props.items[key].price*props.items[key].count}</span>
                                                                    <button className='btn' onClick={() => props.rem(item)}>
                                                                        <img className='btnRemove' src={remuvePic} alt=""/>
                                                                    </button>
                     </span></p>
                </div>: <span></span>}
        </div>)}

        {props.totalPrice > 0? <p className='total'>Total price:<span className='ttpSpan'>{props.totalPrice} KGS</span> </p>
            :<div>
                <p className='orderP'>Order is empty!</p>
                <p className='total'>Order is empty!</p>
            </div> }

    </div>;


export default CreateOrder;