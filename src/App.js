import React, { Component } from 'react';
import './App.css';
import AddItemsHendler from "./components/AddItemsHendler/AddItemsHendler";
import OrderDetail from "./components/OrderDetail/OrderDetail";
class App extends Component {

    state = {
        items: [
            {name: 'Hamburger', count: 0, price: 100},
            {name: 'Cheeseburger', count: 0, price: 120},
            {name: 'Fries', count: 0, price: 50},
            {name: 'Coffee', count: 0, price: 30},
            {name: 'Tea', count: 0, price: 20},
            {name: 'Cola', count: 0, price: 40}

        ],
        totalPrice: 0
    };

    addItemHandler = (iteem) => {
        let items = [...this.state.items];
        let id = items.findIndex(item => item.name === iteem.name);
        items[id].count = items[id].count + 1;


        let totalPrice = this.state.totalPrice;
        totalPrice += iteem.price;

        this.setState({items, totalPrice})
    };

    removeItemHandler = iteem => {
        let items = [...this.state.items];
        let id = items.findIndex(item => item.name === iteem.name);
        items[id].count = items[id].count - 1;

        let totalPrice = this.state.totalPrice;
        totalPrice -= iteem.price;

        this.setState({items, totalPrice})
    };

  render() {
    return (
      <div className="App">
        <AddItemsHendler ingredients={this.state.items} addClickHandler={this.addItemHandler} />
        <OrderDetail items={this.state.items} totalPrice={this.state.totalPrice} rem={this.removeItemHandler} />

      </div>
    );
  }
}

export default App;
